#include "CryptoDevice.h"
#include <md5.h>
#include <hex.h>
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.

std::string encryptAES(std::string);
std::string decryptAES(std::string);

CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];


std::string CryptoDevice::encryptAES(std::string plainText) //This function is encrypting the plain text using the AES method
{
	std::string cipherText;
	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const CryptoPP::byte*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

std::string CryptoDevice::decryptAES(std::string cipherText) //This function is decrypting the plain text using the AES method
{

	std::string decryptedText;
	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const CryptoPP::byte*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}

std::string CryptoDevice::encryptMD5(std::string password) //This function is decrypting the plain text using the MD5 method
{
	CryptoPP::byte digest[CryptoPP::Weak::MD5::DIGESTSIZE];

	CryptoPP::Weak::MD5 hash;
	hash.CalculateDigest(digest, (const CryptoPP::byte*)password.c_str(), password.length());

	CryptoPP::HexEncoder encoder;
	std::string output;

	encoder.Attach(new CryptoPP::StringSink(output));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();

	return output;
}